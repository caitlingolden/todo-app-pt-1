import React from "react";
import TodoList from "./components/todo-list/Todo-List";
//import TodoItem from "../..todo-item/useTodoList";
import todosList from "./todos.json"



export default function App () {

  const [todos, setTodos] = React.useState(todosList);// points to todos.js array of objects
  const [todoInput, setTodoInput] = React.useState("");// "name" or "input" point to the value of input
  ;



  const addTodoItem = (event) => {
      if (event.key === "Enter") {
        
         event.preventDefault();
      let random = Math.random()*100
    const newTodoItem = {
       userId: 1,
       id: random,
       title: todoInput,
       completed: false
     }
     setTodos([...todos, newTodoItem])
    setTodoInput("")
  }
}
  
  function toggleDis(id) {
  const updatedTodos = todos.map((todo) => {
    // if this task has the same ID as the edited task
    if (id === todo.id) {
      // use object spread to make a new object
      // whose `completed` prop has been inverted
      return {...todo, completed: !todo.completed}
    }
    return todo;
  });
  setTodos(updatedTodos);
}


function deleteItem( id){
  const updatedTodos = todos.filter((todo)=>{
    const toKeepItem = id !== todo.id;
    return toKeepItem
  })
  setTodos(updatedTodos)
}

  function clearCompleted( ) {
    const updatedTodos = todos.filter((todo) => {
      const toKeepItem = !todo.completed;
      return toKeepItem;
    });
    setTodos(updatedTodos);
  }

  // TODO: Focus this
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        
        <input 
          className="new-todo"
          placeholder="What needs to be done?"
          onChange={(e) => setTodoInput(e.target.value)}
          onKeyDown={addTodoItem}
          value={todoInput}// title
          autoFocus
        />
        
      </header>
       <TodoList todos={todos} toggleDis={toggleDis} deleteItem={deleteItem} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button className="clear-completed" 
        onClick={() => clearCompleted()}>
          Clear completed</button>
      </footer>
    </section>

  );
}

