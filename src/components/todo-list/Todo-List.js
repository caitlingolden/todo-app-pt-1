import React from 'react';
import TodoItem from "../todo-item/Todo-Item";


  export default function TodoList({ todos, toggleDis, deleteItem }) {
    //let {todos} = props // deconstruction: pulls {todos} out of props
      
    
    return (
        <section className="main">
          <ul className="todo-list">
            {todos.map((todo) => (
              <TodoItem 
              todo={todo} 
              key={todo.title} 
              //completed={completed} 
              toggleDis={toggleDis} 
              deleteItem={deleteItem}/>
            ))}
          </ul>
        </section>
      );
    }
  
 
  //export default TodoList;
  //