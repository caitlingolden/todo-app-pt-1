import React from "react";

export default function TodoItem({ todo, toggleDis, deleteItem }) {
    const { completed, id, title } = todo;
  return (
    <li className={completed ? "completed" : ""}>
      <div className="view">
        <input 
        className="toggle" 
        type="checkbox" 
        checked={completed} 
        onClick= {() => toggleDis(id)} />
      <label>{title}</label>
      <button className="destroy" onClick={() => deleteItem(id)} />
      </div>
    </li>

  )
}