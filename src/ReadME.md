/* class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox" checked={this.props.completed} />
          <label>{this.props.title}</label>
          <button className="destroy" />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title} completed={todo.completed} />
          ))}
        </ul>
      </section>
    );
  }
} 
 */
/* export default App;
class Contact extends Component {
  constructor(props) {
      super(props);
      this.state = {
          submitted: false,
          formData: {
              firstName: '',
              lastName: ''
          }
      };
  }
  handleChange = (event) => {
      const formData = { ...this.state.formData };
      formData[event.target.name] = event.target.value;
      this.setState({ formData })
  }
  handleSubmit = (event) => {
      event.preventDefault();
      this.setState({
          submitted: true
      });
  }
  resetForm = (event) => {
      this.setState({
          submitted: false,
          formData: {
              firstName: '',
              lastName: ''
          }
      });
  }